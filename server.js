const { response } = require("express");
const express = require("express");
const app = express();

app.set("view engine", "ejs");
app.use(express.urlencoded({
  extended: true
}));

let data = [
	{
			"name": "Sofia",
			"temperature": 22,
			"humidity": 67,
			"rain": 17,
			"sky": "Clear"
	},
	{
			"name": "Madrid",
			"temperature": 28,
			"humidity": 53,
			"rain": 0,
			"sky": "Clear"
	},
	{
			"name": "London",
			"temperature": 24,
			"humidity": 72,
			"rain": 92,
			"sky": "Cloudy"
	},
	{
		"name": "Paris",
		"temperature": 24,
		"humidity": 67,
		"rain": 52,
		"sky": "Partly Cloudy"
	}
];

app.get("/", (req, res) => {
		res.redirect("/weather");
});

app.get("/weather", (req, res) => {
	res.render("index", { data });
});

app.post("/weather", (req, res) => {
	
	data.forEach((region) => {
		if(region.name === req.body.regions) {
			res.render("index", { data, show: true, region: region.name, temperature: region.temperature, humidity: region.humidity, rain: region.rain, sky: region.sky });
		};
	});
	
	res.end();
});

app.listen(3000);